package com.kruno.viewpager2

import android.gesture.GestureOverlayView.ORIENTATION_HORIZONTAL
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.core.view.ViewCompat
import androidx.viewpager2.widget.ViewPager2
import com.kruno.viewpager2.adapter.OnboardingAdapter
import com.kruno.viewpager2.models.ViewPagerItem
import com.kruno.viewpager2.utils.ViewPager2PageTransformation
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViewpager()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        initViewpager()
        when (item.itemId) {
            R.id.menu_vertical_orientation -> {
                viewPager.orientation = ViewPager2.ORIENTATION_VERTICAL
            }
            R.id.menu_horizontal_orientation -> {
                viewPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
            }
            R.id.menu_page_transformer -> {
                viewPager.setPageTransformer(ViewPager2PageTransformation())
            }
            R.id.menu_multiple_pages -> {
                setMultipleItem()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initViewpager(){
        val adapter = OnboardingAdapter()
        viewPager.adapter = adapter
        adapter.setItem(data())
        viewPager.setPageTransformer(null)
    }

    private fun setMultipleItem(){
        val pageMarginPx = resources.getDimensionPixelOffset(R.dimen.pageMargin)
        val offsetPx = resources.getDimensionPixelOffset(R.dimen.offset)
        viewPager.setPageTransformer { page, position ->
            val viewPager = page.parent.parent as ViewPager2
            val offset = position * -(2 * offsetPx + pageMarginPx)
            if (viewPager.orientation == ORIENTATION_HORIZONTAL) {
                if (ViewCompat.getLayoutDirection(viewPager) == ViewCompat.LAYOUT_DIRECTION_RTL) {
                    page.translationX = -offset
                } else {
                    page.translationX = offset
                }
            } else {
                page.translationY = offset
            }
        }
    }

    private fun data(): List<ViewPagerItem> {
        return listOf(
            ViewPagerItem(1, "Splash"),
            ViewPagerItem(2, "Home"),
            ViewPagerItem(3, "Whats your number"),
            ViewPagerItem(4, "Verification"),
            ViewPagerItem(5, "Permissions")
        )
    }
}
