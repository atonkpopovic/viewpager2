package com.kruno.viewpager2.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kruno.viewpager2.models.ViewPagerItem

class OnboardingAdapter : RecyclerView.Adapter<OnboardingViewHolder>() {

    var list: List<ViewPagerItem> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OnboardingViewHolder {
        return OnboardingViewHolder(parent)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: OnboardingViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun setItem(list: List<ViewPagerItem>) {
        this.list = list
        notifyDataSetChanged()
    }
}