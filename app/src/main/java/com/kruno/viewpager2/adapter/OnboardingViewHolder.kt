package com.kruno.viewpager2.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kruno.viewpager2.R
import com.kruno.viewpager2.models.ViewPagerItem
import kotlinx.android.synthetic.main.onboarding_item.view.*

class OnboardingViewHolder constructor(itemView: View) :
    RecyclerView.ViewHolder(itemView) {
    constructor(parent: ViewGroup) :
            this(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.onboarding_item,
                    parent,
                    false
                )
            )

    fun bind(item: ViewPagerItem) {
        itemView.tvTitle.text = item.title
    }
}