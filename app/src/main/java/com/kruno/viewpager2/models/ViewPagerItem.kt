package com.kruno.viewpager2.models

class ViewPagerItem  (
    val id: Int? = 0,
    val title: String? = null
)