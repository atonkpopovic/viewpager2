# Android ViewPager2 library
## Implementation
- ViewPager2 implementation with Recyclerview Adapter

## What have changed
- Right-to-left layout support
- Usage of Recyclerview 
- Supports Horizontal and Vertical scrolling
- RegisterOnPageChangeCallback replaces addPageChangeListener

## Cases supported in project
- Vertical orientation
- Horizontal orientation
- Page Transformation
- Multiple Pages with offset